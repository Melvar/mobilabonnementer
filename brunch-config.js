exports.config = {
  // See http://brunch.io/#documentation for docs.
  files: {
    javascripts: {

      // To use a separate vendor.js bundle, specify two files path
      // https://github.com/brunch/brunch/blob/stable/docs/config.md#files
     joinTo: {
        'js/app.js': [/^(web\/static\/js)/,
          /^(deps\/phoenix_html\/web\/static\/js)/,
          /^(deps\/phoenix\/web\/static\/js)/],
        'js/admin_app.js': [/^(web\/static\/js\/admin)/,
            /^(deps\/phoenix_html\/web\/static\/js)/,
            /^(deps\/phoenix\/web\/static\/js)/],
        'js/vendor.js': /^(web\/static\/vendor)/
      },

      // To change the order of concatenation of files, explictly mention here
      // https://github.com/brunch/brunch/tree/master/docs#concatenation
       order: {
         before: [
           'web/static/vendor/jquery-2.1.4.min.js',
           'web/static/vendor/foundation/foundation.js',
           'web/static/vendor/foundation/foundation.slider.js',
          ]
       }
    },
    stylesheets: {
      defaultExtension: 'scss',
      joinTo: {
        'css/app.css':       /^(web\/static\/css\/app)/,
        'css/admin_app.css': /^(web\/static\/css\/admin\/admin_app)/
      }
    },
    templates: {
      joinTo: 'js/app.js'
    }
  },

  conventions: {
    // This option sets where we should place non-css and non-js assets in.
    // By default, we set this to '/web/static/assets'. Files in this directory
    // will be copied to `paths.public`, which is "priv/static" by default.
    assets: /^(web\/static\/assets)/
  },

  // Phoenix paths configuration
  paths: {
    // Dependencies and current project directories to watch
    watched: ["deps/phoenix/web/static",
              "deps/phoenix_html/web/static",
              "web/static", "test/static"],

    // Where to compile files to
    public: "priv/static"
  },

  // Configure your plugins
  plugins: {
    babel: {
      // Do not use ES6 compiler in vendor code
      ignore: [/web\/static\/vendor/]
    },
    sass: {
      mode: 'ruby',
      options: {
        // includePaths: ['web/static/css/vendor']
      }
    }
  },

  modules: {
    autoRequire: {
      'js/app.js': ['web/static/js/app'],
      "js/admin_app.js": ["web/static/js/admin/admin_app"]
    }
  },

  npm: {
    enabled: true
  }
};
