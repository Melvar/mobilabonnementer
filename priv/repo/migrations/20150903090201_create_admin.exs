defmodule Mobil.Repo.Migrations.CreateAdmin do
  use Ecto.Migration

  def change do
    create table(:admins) do
      add :email, :string
      add :crypted_password, :string
      add :name, :string

      timestamps
    end

  end
end
