defmodule Mobil.Repo.Migrations.CreateSubscription do
  use Ecto.Migration

  def change do
    create table(:subscriptions) do
      add :name, :string
      add :price, :float
      add :registration_fee, :float
      add :call_time, :integer
      add :binding_period, :integer
      add :has_4g, :boolean, default: false
      add :endpoint, :string
      add :company_id, :integer

      timestamps
    end
  end
end
