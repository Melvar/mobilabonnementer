defmodule Mobil.Repo.Migrations.AddMissingDataTableToSubscriptions do
  use Ecto.Migration

  def change do
    alter table(:subscriptions) do
      add :data, :integer
    end
  end
end
