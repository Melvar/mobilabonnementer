defmodule Mobil.Repo.Migrations.AddFieldsToSubscriptions do
  use Ecto.Migration

  def change do
    alter table(:subscriptions) do
      add :free_sms, :boolean, default: false
      add :intro_discount, :string
    end
  end
end
