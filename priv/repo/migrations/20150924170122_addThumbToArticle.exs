defmodule Mobil.Repo.Migrations.AddThumbToArticle do
  use Ecto.Migration

  def change do
    alter table(:articles) do
      add :thumb, :string
    end
  end
end
