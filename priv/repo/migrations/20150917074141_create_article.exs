defmodule Mobil.Repo.Migrations.CreateArticle do
  use Ecto.Migration

  def change do
    create table(:articles) do
      add :title, :string
      add :manchet, :text
      add :body, :text
      add :meta_description, :text
      add :published, :boolean, default: false
      add :banner, :string
      add :slug, :string
      add :admin_id, references(:admins)

      timestamps
    end
    create index(:articles, [:admin_id])

  end
end
