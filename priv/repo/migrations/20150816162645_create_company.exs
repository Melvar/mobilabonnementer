defmodule Mobil.Repo.Migrations.CreateCompany do
  use Ecto.Migration

  def change do
    create table(:companies) do
      add :name, :string
      add :logo, :string

      timestamps
    end

  end
end
