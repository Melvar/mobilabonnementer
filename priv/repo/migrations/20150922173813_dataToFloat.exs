defmodule Mobil.Repo.Migrations.DataToFloat do
  use Ecto.Migration

  def change do
    alter table(:subscriptions) do
      modify :data, :float
    end
  end
end
