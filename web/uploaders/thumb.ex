defmodule Mobil.Thumb do
  use Arc.Definition
  use Arc.Ecto.Definition

  @versions [:original, :big_thumb]

  # Whitelist file extensions:
  def validate({file, _}) do
   ~w(.jpg .jpeg .gif .png) |> Enum.member?(Path.extname(file.file_name))
  end

  # Define a thumbnail transformation:
  def transform(:big_thumb, _) do
    {:convert, "-strip -fill white -thumbnail 225x175^ -gravity center -format png"}
  end

  def __storage, do: Arc.Storage.Local

  # Override the persisted filenames:
  def filename(version, _), do: version

  # Override the storage directory:
  def storage_dir(_, {_, scope}) do
    module_name = scope.__struct__
    |> Module.split
    |> List.last
    |> String.downcase
    "uploads/#{module_name}/thumb/#{scope.id}"
  end
end
