defmodule Mobil.Banner do
  use Arc.Definition
  use Arc.Ecto.Definition

  @versions [:original, :banner]

  # Whitelist file extensions:
  def validate({file, _}) do
   ~w(.jpg .jpeg .gif .png) |> Enum.member?(Path.extname(file.file_name))
  end

  # Define a thumbnail transformation:
  def transform(:banner, _) do
    {:convert, "-strip -fill white -thumbnail 790x500^ -gravity center -format png"}
  end

  def __storage, do: Arc.Storage.Local

  # Override the persisted filenames:
  def filename(version, _), do: version

  # Override the storage directory:
  def storage_dir(_, {_, scope}) do
    module_name = scope.__struct__
    |> Module.split
    |> List.last
    |> String.downcase
    "uploads/#{module_name}/banner/#{scope.id}"
  end
end
