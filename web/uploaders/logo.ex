defmodule Mobil.Logo do
  use Arc.Definition
  use Arc.Ecto.Definition

  # To add a thumbnail version:
  @versions [:original, :thumb]

  # Whitelist file extensions:
  def validate({file, _}) do
   ~w(.jpg .jpeg .gif .png) |> Enum.member?(Path.extname(file.file_name))
  end

  # Define a thumbnail transformation:
  def transform(:thumb, _) do
    {:convert, "-strip -fill white -thumbnail 160x90^ -gravity center -format png"}
  end

  def __storage, do: Arc.Storage.Local

  # Override the persisted filenames:
  def filename(version, _), do: version

  # Override the storage directory:
  def storage_dir(_, {_, scope}) do
    module_name = scope.__struct__
    |> Module.split
    |> List.last
    |> String.downcase
    "uploads/#{module_name}/logo/#{scope.id}"
  end

  # Provide a default URL if there hasn't been a file uploaded
  # def default_url(version) do
  #   "/images/avatars/default_#{version}.png"
  # end
end
