defmodule Mobil.Plugs.AdminRestricted do
  import Plug.Conn

  def init(default), do: default

  def call(conn, default) do
    # If the user is authenticated, use admin layout. Else redirect back to login.
    if !Plug.Conn.get_session(conn, :current_user) do
      Phoenix.Controller.redirect(conn, to: "/login")
    else
      Phoenix.Controller.put_layout(conn, {Mobil.LayoutView, "admin_app.html"})
    end
  end
end

defmodule Mobil.Router do
  use Mobil.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :restricted_browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug Mobil.Plugs.AdminRestricted
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Mobil do
    pipe_through :browser

    get "/", SubscriptionController, :index
    get "/login", SessionController, :new
    post "/login", SessionController, :create
    delete "/logout", SessionController, :delete

    resources "/companies", CompanyController
    resources "/subscriptions", SubscriptionController
    resources "/articles", ArticleController
  end

  scope "/admin", Mobil.Admin, as: :admin do
    pipe_through :restricted_browser

    get "/", SubscriptionController, :index
    resources "/companies", CompanyController
    resources "/subscriptions", SubscriptionController
    resources "/articles", ArticleController
  end
end
