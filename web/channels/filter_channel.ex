defmodule Mobil.FilterChannel do
  use Phoenix.Channel

  alias Mobil.Subscription

  def join("filter:lobby", auth_msg, socket) do
    {:ok, socket}
  end

  def handle_in("new_filter", %{"filters" => filters, "page" => page}, socket) do
    subscriptions = Subscription.by_filter(filters |> Map.to_list, page, 7)
    |> Enum.sort_by(&Subscription.min_price/1)

    # Pick values from subscriptions, and add some computed values.
    subscriptions_payload = subscriptions |> Enum.map fn(s) ->
      values = Map.take(s, subscription_params)
      values = Map.put_new(values, :image_path, Mobil.Logo.url({s.company.logo, s.company}, :thumb) |> String.replace "priv/static/", "")
      values = Map.put_new(values, :min_price, Subscription.min_price(s))
    end

    if page == 1 do
      push socket, "new_filter", %{subscriptions: subscriptions_payload}
    else
      push socket, "filter_next_page", %{subscriptions: subscriptions_payload}
    end
    {:noreply, socket}
  end

  def subscription_params do
    [:id, :name, :registration_fee, :call_time, :binding_period, :has_4g, :endpoint, :data, :company_id, :price, :free_sms]
  end
end
