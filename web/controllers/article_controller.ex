defmodule Mobil.ArticleController do
  use Mobil.Web, :controller

  alias Mobil.Article

  def index(conn, _params) do
    articles = Repo.all(Article)
    render(conn, "index.html", articles: articles)
  end

  def show(conn, %{"id" => id}) do
    article = Repo.get_by!(Article, slug: id)
    render(conn, "show.html", article: article, page_title: article.title, meta_description: article.meta_description)
  end
end
