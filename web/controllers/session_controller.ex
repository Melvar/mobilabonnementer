defmodule Mobil.SessionController do
    use Mobil.Web, :controller

    def new(conn, _params) do
      render conn, "new.html"
    end

    def create(conn, %{"session" => session_params}) do
      case Mobil.Session.login(session_params, Repo) do
        {:ok, admin} ->
          conn
          |> put_session(:current_user, admin.id)
          |> put_flash(:info, "Authenticated")
          |> redirect(to: "/admin")
        :error ->
          conn
          |> put_flash(:info, "Wrong email or password")
          |> render("new.html")
      end

    end
end
