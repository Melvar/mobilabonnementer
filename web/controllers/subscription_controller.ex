defmodule Mobil.SubscriptionController do
  use Mobil.Web, :controller

  alias Mobil.Subscription
  alias Mobil.Company

  plug :scrub_params, "subscription" when action in [:create, :update]

  def index(conn, _params) do
    query = from s in Subscription, limit: 7
    subscriptions = Repo.all(query)
    |> Repo.preload([:company])
    |> Enum.sort_by(&Subscription.min_price/1)
    render(conn, "index.html", subscriptions: subscriptions)
  end

  def show(conn, %{"id" => id}) do
    subscription = Repo.get!(Subscription, id)
    render(conn, "show.html", subscription: subscription)
  end
end
