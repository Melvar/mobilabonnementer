defmodule Mobil.ArticleView do
  use Mobil.Web, :view

  def correct_image_path(path) do
    String.replace path, "priv/static/", "/"
  end
end
