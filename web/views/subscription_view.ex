defmodule Mobil.SubscriptionView do
  use Mobil.Web, :view

  def correct_image_path(path) do
    String.replace path, "priv/static/", "/"
  end

  def format_price(price) do
    price
    |> Float.to_string [decimals: 0]
  end
end
