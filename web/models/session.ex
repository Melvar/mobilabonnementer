defmodule Mobil.Session do
  alias Mobil.Admin
  alias Mobil.Repo

  def login(params, repo) do
    admin = repo.get_by(Admin, email: String.downcase(params["email"]))
    case authenticate(admin, params["password"]) do
      true -> {:ok, admin}
      _    -> :error
    end
  end

  def authenticate(admin, password) do
    case admin do
      nil -> false
      _   -> Comeonin.Bcrypt.checkpw(password, admin.crypted_password)
    end
  end


  def current_user(conn) do
    id = Plug.Conn.get_session(conn, :current_user)
    if id, do: Repo.get(Admin, id)
  end

  def logged_in?(conn), do: !!current_user(conn)
end
