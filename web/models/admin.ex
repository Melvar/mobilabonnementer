defmodule Mobil.Admin do
  use Mobil.Web, :model

  before_insert :hash_password

  schema "admins" do
    field :email, :string
    field :crypted_password, :string
    field :password, :string, virtual: true
    field :name, :string

    timestamps
  end

  @required_fields ~w(email name crypted_password)
  @optional_fields ~w()

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end

  defp hash_password(changeset) do
    changeset
    |> Ecto.Changeset.put_change(:crypted_password, Comeonin.Bcrypt.hashpwsalt(changeset.params["password"]))
  end
end
