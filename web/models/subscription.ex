defmodule Mobil.Subscription do
  use Mobil.Web, :model
  @derive [Poison.Encoder]

  alias Mobil.Subscription
  alias Mobil.Repo

  schema "subscriptions" do
    field :name, :string
    field :price, :float
    field :registration_fee, :float
    field :call_time, :integer
    field :binding_period, :integer
    field :has_4g, :boolean, default: false
    field :free_sms, :boolean, default: false
    field :intro_discount, :string
    field :endpoint, :string
    field :data, :float

    belongs_to :company, Mobil.Company

    timestamps
  end

  @required_fields ~w(price registration_fee call_time binding_period has_4g endpoint company_id data free_sms)
  @optional_fields ~w(name endpoint)

  def min_price(subscription) do
    if subscription.binding_period == 0 do
      subscription.price + subscription.registration_fee
    else
      subscription.binding_period * subscription.price + subscription.registration_fee
    end
  end

  def get_max_value(column) do
    {:ok, res} = Ecto.Adapters.SQL.query(Repo, "SELECT max($1) FROM subscriptions", [column])
    res.rows |> hd |> hd
  end

  def by_filter(filters, page, count) do
    query = from(c in Subscription, select: c, preload: [:company])
    query = construct_filter_query(filters, query)

    # Add limit, and offset.
    offset = (page - 1) * count
    query = from s in query, limit: ^count, offset: ^offset
    Repo.all(query)
  end

  def construct_filter_query([], query), do: query
  def construct_filter_query([head | tail], query) do
    column = elem(head, 0) |> String.to_atom
    query = from s in query,
      where: field(s, ^column ) >= ^elem(head, 1)

    construct_filter_query(tail, query)
  end

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end
end
