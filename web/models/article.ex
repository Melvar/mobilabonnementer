defmodule Mobil.Article do
  use Mobil.Web, :model
  use Arc.Ecto.Model

  schema "articles" do
    field :title, :string
    field :manchet, :string
    field :body, :string
    field :meta_description, :string
    field :published, :boolean, default: false
    field :banner, Mobil.Banner.Type
    field :thumb, Mobil.Thumb.Type
    field :slug, :string
    belongs_to :admin, Mobil.Admin

    timestamps
  end

  @required_fields ~w(title body meta_description published slug)
  @optional_fields ~w(manchet)

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
    |> cast_attachments(params, ~w(), ~w(banner thumb))
  end

  # Used on create, where the id is not yet present in the model, and must therefor be called after model save.
  def file_changeset(model, params \\ :empty) do
    model
    |> cast_attachments(params, ~w(), ~w(banner thumb))
  end
end
