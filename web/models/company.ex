defmodule Mobil.Company do
  use Mobil.Web, :model
  use Arc.Ecto.Model

  schema "companies" do
    field :name, :string
    field :logo, Mobil.Logo.Type

    timestamps
  end

  @required_fields ~w(name)
  @optional_fields ~w()

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
    |> cast_attachments(params, ~w(), ~w(logo))
  end

  def file_changeset(model, params \\ :empty) do
    model
    |> cast_attachments(params, ~w(), ~w(logo))
  end
end
