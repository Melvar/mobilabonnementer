import {productTemplate} from "./product_template";

export class Pager {
  constructor(chan, filter) {
    this.products_on_page = 7;
    this.current_page = 1;
    this.filter = filter;
    this.chan = chan;

    // Bind event.
    $('#pager').click(e => {
      e.preventDefault();
      this.nextPage();
    });

    // When getting the next page from the server.
    this.chan.on("filter_next_page", payload => {
      ga('send', 'pageview');
      this.applyPage(payload);

      // If the payload doesn't contain the expect number of items, it's considered the last page.
      if (payload.subscriptions.length != this.products_on_page) {
        this.lastPage();
      }
    });

    // Reset pager state, when new filter is applied.
    this.chan.on("new_filter", payload => {
      this.reset(payload);
    });
  }

  nextPage() {
    this.current_page += 1;

    this.chan.push("new_filter", {
      filters: this.filter.getFilters(),
      page: this.current_page
    });
  }

  lastPage() {
    $('#pager').animo( { animation: 'fadeOut', duration: 0.4 }, () => {
      $('#pager').hide();
    });
  }

  reset(payload) {
    this.current_page = 1;

    if (payload.subscriptions.length !== 0) {
      $('#pager').show().animo( { animation: 'fadeIn', duration: 0.4 });
    }
  }

  applyPage(data) {
    var enterFrontend = function(subscription, timeout) {
      setTimeout(function() {
        var template = $(tempFn(subscription));
        $(template).insertAfter($('.site-outlet > .product-list').last());
        $(template).animo( { animation: 'flipInX', duration: 0.4 } );
      }, timeout)
    }

    // Add each subscriptions to the frontend.
    var tempFn = doT.template(productTemplate());
    var timeout = 0;
    $.each(data.subscriptions, (i, subscription) => {
      timeout += 100;

      // Format prices.
      subscription.price = Math.round(subscription.price);
      subscription.registration_fee = Math.round(subscription.registration_fee);
      subscription.min_price = Math.round(subscription.min_price);
      enterFrontend(subscription, timeout);
    });

  }
}
