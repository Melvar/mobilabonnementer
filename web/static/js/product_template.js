
export function productTemplate() {
  return `
  <div class="product-list">
    <div class="product-list__image center">
      <img src="{{=it.image_path}}">
    </div>

    <div class="big-icon-header">
      <div class="big-icon-header__icon">
        <img src="images/hours.png">
      </div>

      <hr class="big-icon-header__byline">
      <div class="big-icon-header__text">
        Taltid

        <span class="big-icon-header__value">
          {{? it.call_time === 1000}}
            <strong>FRIA</strong>
          {{??}}
            <strong>{{= it.call_time }}</strong> Samtal
          {{?}}
        </span>
      </div>
    </div>

    <div class="big-icon-header">
      <div class="big-icon-header__icon">
        <img src="images/data.png">
      </div>

      <hr class="big-icon-header__byline">
      <div class="big-icon-header__text">
        Data
        <span class="big-icon-header__value">
          <strong>{{=it.data}}</strong> GB
        </span>
      </div>
    </div>

    <div class="option-box">
      <div class="option-box__header">
        Möjligheter
      </div>
      <div class="option-box__cell">
        <div class="option-box__icon">
          <img src="images/sms.png">
        </div>
        <div class="option-box__text">
          SMS
          <span class="option-box__value">
            {{? it.free_sms }}
              <strong>Fria</strong>
            {{??}}
            {{?}}
          </span>
        </div>
        <hr class="option-box__byline">
      </div>
      <div class="option-box__cell">
        <div class="option-box__icon">
          <img src="images/binding.png">
        </div>
        <div class="option-box__text">
          <span class="option-box__value">
            {{? it.binding_period === 0 }}
              <strong>Ingen</strong>
            {{??}}
              <strong>{{= it.binding_period }}</strong> Mån.
            {{?}}
          </span>
        </div>
       <hr class="option-box__byline">
      </div>
      <div class="option-box__cell">
        <div class="option-box__icon">
          <img src="images/connection.png">
        </div>
        <div class="option-box__text">
          <span class="option-box__value">
            {{? it.has_4g }}
              <strong>Ja</strong>
            {{??}}
              3G
            {{?}}
          </span>
        </div>
      </div>
      <div class="option-box__cell">
        <div class="option-box__icon">
          <img src="images/registration.png">
        </div>
        <div class="option-box__text">
          <span class="option-box__value">
            <strong>{{= it.registration_fee }}:-</strong>
          </span>
        </div>
      </div>
    </div>
    <div class="product-list__price">
      <strong class="product-list__price__span">kr/mån</strong><strong class="header--huge pull-right">{{= it.price }}:-</strong>

      <div class="product-list__price__total">
        Minsta pris  <strong>{{= it.min_price}}:-</strong>
      </div>

      <a href="{{= it.endpoint }}" target="_blank" class="btn btn--success btn--big fluid">Välj</a>
    </div>
  </div>
  `;
}
