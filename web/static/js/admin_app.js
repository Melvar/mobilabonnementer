import "deps/phoenix_html/web/static/js/phoenix_html"

export var AdminApp = {
  run: () => {

    // Menu slide up.
    var $menuLinks = $('.sidebar .nav a');

    $menuLinks.each( (i, a) => {
      if ($(a).attr('href') == location.pathname) {
        $(a).parents('.nav-second-level').show().addClass('active');
      }
    });

    $('.sidebar .nav:not(.nav-second-level) > li > a').click( function(e) {
      e.preventDefault();

      var $nav = $(this).next();

      if ($nav.hasClass('active')) {
        $nav.removeClass('active');
        $nav.slideUp();
      } else {
        $nav.addClass('active');
        $nav.slideDown();
      }
    });
  }
}
