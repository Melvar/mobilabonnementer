
import {productTemplate} from "./product_template";

export class ProductFilter {
  constructor(chan) {
    this.hours = 0;
    this.data = 0;
    this.started = false;
    this.chan = chan;

    this.initListeners();
    // Bind events to filters.
    var self = this;
    $('[data-slider]').on('change.fndtn.slider', $.debounce(1, function() {
      var $this = $(this);

      // Only push if the value actually changed.
      if ($this.attr('id') == 'filter-data' && self.data != $this.attr('data-slider')) {
        self.data = $this.attr('data-slider');
      }
      if ($this.attr('id') == 'filter-hours' && self.hours != $this.attr('data-slider')) {
        self.hours = $this.attr('data-slider');
        // 1000 hours, free hours. 600 is max of slider, therfor free hours.
        if (self.hours == 600) {
          self.hours = 1000;
          $('#sliderHours').html('Fria');
        }
      }
    }));

    // Only trigger filters, after the user have touched the sliders.
    $('[data-slider]').on('mousedown.fndtn.slider touchstart.fndtn.slider pointerdown.fndtn.slide', () => { self.started = true; });
    $('[data-slider]').on('change.fndtn.slider', $.debounce(300, false, (test) => {
      if (!this.started) {
        return false;
      }

      self.pushFilters();
    }));

    $('input[type=checkbox]').change( () => {
      self.pushFilters();
    });
  }

  // Connect to filter server.
  initListeners() {
    // When getting filter result back from the server.
    this.chan.on("new_filter", payload => {
      ga('send', 'pageview');
      this.rebuild(payload);
    });
  }

  // Fetches all filters; is ran on every filter change.
  getFilters() {
    var filters = {
      call_time: this.hours,
      data: this.data
    };

    if ($('#filter-free-sms').is(':checked')) filters['free_sms'] = true;
    if ($('#filter-no-contract').is(':checked')) filters['binding_period'] = 0;
    if ($('#filter-free-registration').is(':checked')) filters['registration_fee'] = 0.0;
    if ($('#filter-4g').is(':checked')) filters['has_4g'] = true;

    return filters;
  }

  // Called whenever ever the user modified the filters.
  pushFilters(chan) {
    this.chan.push("new_filter", {
      filters: this.getFilters(),
      page: 1
    });
  }

  // Rebuilds product list after filtering
  rebuild(data) {
    // If the result yielded no result, tell the user.
    if (data.subscriptions.length == 0) {
      $('#pager').hide();
      $('#no-result').show().animo({ animation: 'fadeInUp', duration: 0.4 });
    } else {
      $('#no-result').hide();
      $('#pager').show().animo({ animation: 'fadeInUp', duration: 0.4 });
    }

    var buildMarkup = function() {
      var enterFrontend = function(subscription, timeout) {
        setTimeout(function() {
          var template = $(tempFn(subscription));
          $(template).insertBefore('.site-outlet #pager');
          $(template).animo( { animation: 'flipInX', duration: 0.4 } );
        }, timeout)
      }

      // Add each subscriptions to the frontend.
      var tempFn = doT.template(productTemplate());
      var timeout = 0;
      $.each(data.subscriptions, (i, subscription) => {
        timeout += 100;

        // Format prices.
        subscription.price = Math.round(subscription.price)
        subscription.registration_fee = Math.round(subscription.registration_fee)
        subscription.min_price = Math.round(subscription.min_price)
        enterFrontend(subscription, timeout);
      });
    };

    var currentSubscriptions = $('.product-list');
    // If there is subscriptions in the list, fade and clear them.
    if (currentSubscriptions.size() > 0) {
      currentSubscriptions.animo( { animation: 'fadeOutUp', duration: 0.2 }, function() {
        $('.site-outlet .product-list').remove();
        buildMarkup();
      });
    } else {
      buildMarkup();
    }
  }
}
