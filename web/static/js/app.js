// Brunch automatically concatenates all files in your
// watched paths. Those paths can be configured at
// config.paths.watched in "brunch-config.js".
//
// However, those files will only be executed if
// explicitly imported. The only exception are files
// in vendor, which are never wrapped in imports and
// therefore are always executed.

// Import dependencies
//
// If you no longer want to use a dependency, remember
// to also remove its path from "config.paths.watched".
import "deps/phoenix_html/web/static/js/phoenix_html"
import {Socket} from "deps/phoenix/web/static/js/phoenix";
import { ProductFilter } from "./filter";
import { Pager } from "./pager";

export var App = {
  run: () => {
    // Track click conversion, to companies.
    $('.product-list__price a').click( () => { ga('send', 'event', 'convert', 'company') });

    // Connect to server.
    let socket = new Socket("/socket");
    socket.connect();
    var chan = socket.channel("filter:lobby", {});
    chan.join().receive("ok", resp => {
      console.log("Joined server.");
    });

    // Bootstrap modules.
    $(document).foundation();
    var productFilter = new ProductFilter(chan);
    var pager = new Pager(chan, productFilter);
  }
}
