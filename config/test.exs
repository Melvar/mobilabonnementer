use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :mobil, Mobil.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :mobil, Mobil.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "mobil_test",
  pool: Ecto.Adapters.SQL.Sandbox
