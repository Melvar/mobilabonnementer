cd /code
npm install
mix deps.get --only prod
MIX_ENV=prod mix compile

brunch build --production
MIX_ENV=prod mix phoenix.digest
MIX_ENV=prod mix ecto.migrate

MIX_ENV=prod PORT=$PHOENIX_PORT  mix phoenix.server
