defmodule Mobil.ArticleTest do
  use Mobil.ModelCase

  alias Mobil.Article

  @valid_attrs %{banner: "some content", body: "some content", manchet: "some content", meta_description: "some content", published: true, slug: "some content", title: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Article.changeset(%Article{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Article.changeset(%Article{}, @invalid_attrs)
    refute changeset.valid?
  end
end
