defmodule Mobil.SubscriptionTest do
  use Mobil.ModelCase

  alias Mobil.Subscription

  @valid_attrs %{binding_period: 42, call_time: 42, company_id: 42, endpoint: "some content", has_4g: true, name: "some content", price: "120.5", registration_fee: "120.5"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Subscription.changeset(%Subscription{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Subscription.changeset(%Subscription{}, @invalid_attrs)
    refute changeset.valid?
  end
end
